package service

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestService_NewService(t *testing.T) {
	prodMock := new(MockProducer)
	presMock := new(MockPresenter)

	s := NewService(prodMock, presMock)

	if s == nil {
		t.Error("NewService returned nil")
	}

	if s.prod != prodMock || s.pres != presMock {
		t.Error("NewService did not set dependencies correctly")
	}
}

func TestService_Contains(t *testing.T) {
	tests := []struct {
		name   string
		s      string
		substr string
		want   bool
	}{
		{name: "ok", s: "asdAAA", substr: "AA", want: true},
		{name: "empty string", s: "", substr: "", want: true},
		{name: "empty string", s: "asd", substr: "", want: true},
		{name: "len s < len string", s: "asd", substr: "asdasd", want: false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := contains(tt.s, tt.substr)
			require.Equal(t, tt.want, got, "Result not as expected")
		})
	}
}

func TestService_SpamMasker(t *testing.T) {
	prodMock := new(MockProducer)
	presMock := new(MockPresenter)
	prodMock.On("Produce").Return([]string{"input"}, nil)
	presMock.On("Present", []string{"output"}).Return(nil)

	type args struct {
		input string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{name: "good query", args: args{
			"Here's my spammy page: http://hehefouls.netHAHAHA see you.",
		}, want: "Here's my spammy page: http://******************* see you.", wantErr: false},

		{name: "empty input string", args: args{
			"",
		}, want: "", wantErr: true},

		{name: "empty input url", args: args{
			"Here's my spammy page: http:// see you.",
		}, want: "", wantErr: true},

		{name: "empty input start", args: args{
			"//hehefouls.netHAHAHA see you.",
		}, want: "", wantErr: true},

		{name: "empty input end", args: args{
			"Here's my spammy page: http:// hahaha",
		}, want: "", wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := Service{
				prod: prodMock,
				pres: presMock,
			}
			got, err := s.SpamMasker(tt.args.input)

			require.Equal(t, tt.want, got, "Result not as expected")

			if tt.wantErr {
				assert.Error(t, err, "Expected error but got none")
			} else {
				assert.NoError(t, err, "Unexpected error")
			}
		})
	}

}

func TestService_Run(t *testing.T) {
	prodMock := new(MockProducer)
	presMock := new(MockPresenter)

	s := NewService(prodMock, presMock)

	prodMock.On("Produce").Return([]string{"Here's my spammy page: http://example.com see you."}, nil)
	presMock.On("Present", mock.Anything).Return(nil)

	err := s.Run()

	assert.NoError(t, err)

	prodMock.AssertCalled(t, "Produce")
	presMock.AssertCalled(t, "Present", mock.Anything)
}
