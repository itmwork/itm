package service

import (
	"fmt"
	"strings"
)

type Producer interface {
	Produce() ([]string, error)
}
type Presenter interface {
	Present([]string) error
}

type Service struct {
	prod Producer
	pres Presenter
}

func NewService(prod Producer, pres Presenter) *Service {
	return &Service{prod, pres}
}

func (s *Service) Run() error {
	data, err := s.prod.Produce()
	if err != nil {
		return fmt.Errorf("run(): %w", err)
	}

	for i, str := range data {
		data[i], err = s.SpamMasker(str)
		if err != nil {
			return fmt.Errorf("run() SpamMasker(): %w", err)
		}
	}

	err = s.pres.Present(data)
	if err != nil {
		return fmt.Errorf("run(): %w", err)
	}
	return nil
}

func (s Service) SpamMasker(input string) (string, error) {

	var builder strings.Builder
	builder.Grow(len(input))

	tempStart := "Here's my spammy page: http://"
	tempEnd := " see you."

	if !contains(input, tempStart) || !contains(input, tempEnd) {
		return "", fmt.Errorf("incorrect query")
	}

	builder.WriteString(tempStart)

	lenQuery := len(input) - len(tempStart) - len(tempEnd)
	if lenQuery == 0 {
		return "", fmt.Errorf("empty query")
	}

	for i := 0; i < lenQuery; i++ {
		builder.WriteRune('*')
	}
	builder.WriteString(tempEnd)

	return builder.String(), nil
}

func contains(s, substr string) bool {
	subLen := len(substr)

	for i := 0; i <= len(s)-subLen; i++ {
		if s[i:i+subLen] == substr {
			return true
		}
	}
	return false
}
