package presenter

import (
	"fmt"
	"os"
	"strings"
)

type FilePresenter struct {
	FilePath string
}

func (r *FilePresenter) Present(data []string) error {

	output := strings.Join(data, "\n")
	err := os.WriteFile(r.FilePath, []byte(output), 0777)
	if err != nil {
		return fmt.Errorf("present() WriteFile():%w", err)
	}
	return nil
}
