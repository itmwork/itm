package producer

import (
	"fmt"
	"os"
	"strings"
)

type FileProducer struct {
	FilePath string
}

func (r *FileProducer) Produce() ([]string, error) {
	data, err := os.ReadFile(r.FilePath)
	if err != nil {
		return nil, fmt.Errorf("produce() ReadFile():%w", err)
	}
	return strings.Split(string(data), "\n"), nil
}
