package main

import (
	"fmt"
	"gitlab.com/itmwork/itmproject.git/GO_95/presenter"
	"gitlab.com/itmwork/itmproject.git/GO_95/producer"
	"gitlab.com/itmwork/itmproject.git/GO_95/service"
	"os"
)

const fileIn = "fileIn.txt"
const defaultPath = "fileOutDefault.txt"

func main() {
	fileOut := defaultPath
	if len(os.Args) > 1 {
		fileOut = os.Args[1]
	}

	prod := producer.FileProducer{FilePath: fileIn}
	pres := presenter.FilePresenter{FilePath: fileOut}

	servData := service.NewService(&prod, &pres)
	err := servData.Run()
	if err != nil {
		fmt.Printf("Run():%v", err)
	}
}
